import 'package:drawer_flutter/navigationDrawer/navigationDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class eventPage extends StatelessWidget {
  static const String routeName = '/eventPage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("Thank You for Visiting US"),
        ),
        drawer: navigationDrawer(),
        body: Center(
            child: Text(
          "Good Luck, Please Have a Nice Day.",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 21,
            color: Colors.red,
            backgroundColor: Colors.lime,
          ),
        )));
  }
}
