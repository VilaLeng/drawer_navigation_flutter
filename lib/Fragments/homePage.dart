import 'package:drawer_flutter/navigationDrawer/navigationDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class homePage extends StatelessWidget {
  static const String routeName = '/homePage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      drawer: navigationDrawer(),
      // body: Center(child: Text("This is home page"))
      body: Column(
        children: [
          Row(
            children: [
              // CircleAvatar(
              //   radius:168,
              //   backgroundImage: AssetImage('assets/images/japan.jpg'),
              // ),
              Image.asset('assets/images/japan.jpg', width: 381, height: 355, scale: 0.8,),
            ],
          ),
          //Text('Welcome to Home Page')
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 30),
            child: TextField(
              //controller: myController_pwd,
              style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
              decoration: InputDecoration(
                hintText: 'Welcome to Home Page 0030...',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 30),
            child: TextField(
              //controller: myController_pwd,
              style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,),
              decoration: InputDecoration(
                hintText: 'Nothing is Perfect on the First Time.',
              ),
            ),
          ),


        ],
      ),

    );
  }
}