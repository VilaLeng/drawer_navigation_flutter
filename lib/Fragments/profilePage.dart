import 'package:drawer_flutter/navigationDrawer/navigationDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class profilePage extends StatelessWidget {
  static const String routeName = '/profilePage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("My Profile Page"),
      ),
      drawer: navigationDrawer(),
      //  body: Center(child: Text("This is profile page"))
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 20,
        ),
        color: Colors.grey[200],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundImage: AssetImage('assets/images/vila.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Mr. Vila Leng',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Software Developer',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.flutter_dash,
                    size: 58,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Mr. Vila Leng, you can call me Win. I am a junior student at Prince of Songkla University'
                      ', Trang Campus. My Major is Information and Computer Management.'
                      'I will be a skillful Software Developer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
        Divider(
            thickness: 0.8,
            color: Colors.blueGrey,
          ),
              Padding(
                padding: EdgeInsets.only(
                  left: 0,
                  right: 130,
                  top: 10,
                  bottom: 10
                ),
                child: Text(
                  'My Contact Information:',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 205,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'Tel: 092506567',
                  style: TextStyle(
                      fontSize: 15,
                      //fontWeight: FontWeight.bold,
                      color: Colors.blueGrey[69]),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 38,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'Email Adress: 6250110030@gmail.com',
                  style: TextStyle(
                      fontSize: 15,
                      //fontWeight: FontWeight.bold,
                      color: Colors.blueGrey[69]),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
