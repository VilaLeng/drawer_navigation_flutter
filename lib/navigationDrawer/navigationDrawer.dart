import 'package:drawer_flutter/routes/pageRoute.dart';
import 'package:drawer_flutter/widgets/createDrawerBodyItem.dart';
import 'package:drawer_flutter/widgets/createDrawerHeader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class navigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.blueGrey[200],
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(
            icon: Icons.home,
            text: 'Home Page',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.home),
          ),
          // Divider(
          //   thickness: 0.8,
          //   color: Colors.blue,
          // ),

          createDrawerBodyItem(
            icon: Icons.account_circle,
            text: 'Profile Page',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.profile),
          ),
          //
          // createDrawerBodyItem(
          //   icon: Icons.event_note,
          //   text: 'Events',
          //   onTap: () =>
          //       Navigator.pushReplacementNamed(context, pageRoutes.event),
          // ),
          // //Divider(),
          // createDrawerBodyItem(
          //   icon: Icons.notifications_active,
          //   text: 'Notifications',
          //   onTap: () =>
          //       Navigator.pushReplacementNamed(context, pageRoutes.notification),
          // ),
          // createDrawerBodyItem(
          //   icon: Icons.contact_phone,
          //   text: 'Contact Info',
          //   onTap: () =>
          //       Navigator.pushReplacementNamed(context, pageRoutes.contact),
          // ),
          // Divider(
          //   thickness: 0.8,
          //   color: Colors.blue,
          // ),
          //
          // ListTile(
          //   title: Text('App version 1.0.0'),
          //   onTap: () {},//onTap
          // ),
          createDrawerBodyItem(
            //icon: Icons.contact_phone,
            icon: Icons.exit_to_app,
            text: 'Log Out',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.event),
          ),
        ],
      ),
    );
  }
}
