import 'package:drawer_flutter/Fragments/contactPage.dart';
import 'package:drawer_flutter/Fragments/eventPage.dart';
import 'package:drawer_flutter/Fragments/homePage.dart';
import 'package:drawer_flutter/Fragments/notificationPage.dart';
import 'package:drawer_flutter/Fragments/profilePage.dart';

class pageRoutes {
  static const String home = homePage.routeName;
  static const String contact = contactPage.routeName;
  static const String event = eventPage.routeName;
  static const String profile = profilePage.routeName;
  static const String notification = notificationPage.routeName;
}